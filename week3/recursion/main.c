#include <stdio.h>
#include <stdlib.h>

int nrDigits(int num){
    static int count = 1;
    if(num<10 ){
        return count;
    }
    else{
        num = num/10;
        count++;
        return nrDigits(num);
    }


}

int GCD(int num1, int num2){
    int temp;
    if(num1<num2){
    temp = num1;
    num1 = num2;
    num2 = temp;
    }
    else if(num1%num2 ==0){
        return num2;
    }
    else{
        return GCD(num2, num1%num2);
    }
}
// If the number is prime thiss function will return 1 otherwise will return 1.
int isPrime(int num1,int num2){
    if(num2==1){
        return 1;
    }
    else if(num1%num2==0){
        return 0;
    }
    else{
        return isPrime(num1,num2-1);
    }
    return 1;
}

int main()
{
/* QUESTION 1

    int num;
    printf("Enter a positive integer number: ");
    scanf("%d",&num);
    printf("Total digits in number %d is: %d ", num, nrDigits(num));
*/

/* QUESTION 2

    int num1;
    int num2;

    printf("Enter the first integer: ");
    scanf("%d", &num1);
    printf("Enter the second integer: ");
    scanf("%d", &num2);
    printf("Greatest common divisor of two integers is: %d", GCD(num1,num2));

*/

/* QUESTION 3

    printf("%d",isPrime(33,16));

    return 0;
    */

}


